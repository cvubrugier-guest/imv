imv (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0 (closes: #959095).
  * Install the /etc/imv_config file.
  * d/control:
    - Bump Standards-Version to 4.5.0 (no changes needed).
    - Bump the dh compat level to 13.
    - Do not disable dh_auto_test: the tests now pass in a schroot
      environment (sbuild), let's try enabling them.
    - Drop `make doc` from override_dh_auto_install: not needed as the `all`
      target already builds `doc`.
    - Use the paride@debian.org alias in Uploaders.
  * Drop upstreamed patches:
    - 0002-Fix-typo-in-manpages.patch
    - 0003-Fix-the-name-of-the-GNU-LGPL.patch
  * Switch to a "patches applied + git debcherry" packaging workflow.

 -- Paride Legovini <paride@debian.org>  Sun, 17 May 2020 22:22:50 +0000

imv (4.0.1-1) unstable; urgency=medium

  * New upstream version 4.0.1 (closes: #905693).
  * Upstream switched from dual licensing (Expat and GPL-2+) to Expat only.
    Update d/copyright accordingly.
  * d/copyright: add the new contributors.
  * Bump Standards-Version to 4.4.1 (no changes needed).
  * Build-Depend on debhelper-compat and drop d/compat.
  * d/rules: drop DEB_BUILD_MAINT_OPTIONS = hardening=+all.
    ld --as-needed is enabled by default on Debian development versions.
  * Update Build-Depends due to upstream changes:
     - add: libegl1-mesa-dev, libglu1-mesa-dev, librsvg2-dev,
            libwayland-dev, libxkbcommon-x11-dev;
     - del: libsdl2-dev, libsdl2-ttf-dev.
  * Skip the install target and install everything with debhelper.
    New Build-Depend: dh-exec.
  * d/control: add Rules-Requires-Root: no
  * Explain the new way to invoke imv. Changes:
     - update d/README.Debian;
     - update the package description in d/control; and
     - add d/NEWS explaining how the binaries are named.
  * Provide a imvr -> imv-x11 symlink for legacy support.
  * Refresh and add new patches. Refreshed:
     - 0001-Build-against-the-Debian-packaged-libinih.patch
    New:
     - 0002-Fix-typo-in-manpages.patch (cherry-picked)
     - 0003-Fix-the-name-of-the-GNU-LGPL.patch (submitted upstream)
     - 0004-desktop-file-call-imv-x11.patch

 -- Paride Legovini <pl@ninthfloor.org>  Sat, 30 Nov 2019 18:46:48 +0000

imv (3.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #898220)

 -- Paride Legovini <pl@ninthfloor.org>  Mon, 07 May 2018 15:41:49 +0000
